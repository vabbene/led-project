#include <SoftwareSerial.h>
#include <Wire.h>
int pinLed = 3; // regula la lamapa led
int pinLedBT = 6; // Se prende cuando el BT se activa
int pinInicio = 4; // alimenta lampara Led
int pinPote = A0; // lee el potenciometro
int Val;
int ValOld = 0;
int pote;
const byte BTpin = 7; //pin de estado del BT
SoftwareSerial mySerial(8, 9); // RX, TX lecturas del BT
//void bluetooth();
void setup() {
  // put your setup code here, to run once:
  pinMode(pinLed, OUTPUT); // declara el pin como salida
  pinMode(pinLedBT, OUTPUT); // declara el pin como salida
  pinMode(pinInicio, OUTPUT); // declara el pin como salida
  pinMode(BTpin, INPUT); // declara el pin como entrada
  Serial.begin(9600);
  mySerial.begin(9600);
  Val = map(analogRead(pinPote), 0, 1023.0, 0, 255);
  analogWrite(pinLed, Val);
  delay(30);
  digitalWrite(pinInicio, HIGH); // prende la lampara
}

void loop() {
  // put your main code here, to run repeatedly:
  Val = map(analogRead(pinPote), 0, 1023.0, 0, 255);
  if (Val != ValOld && mySerial.read() != 'A')
  {
    if (Val < 5) Val = 0;
    if (Val > 252) Val = 255;
    analogWrite(pinLed, Val);
    //Serial.println(Val);
    ValOld  = Val;
  }

  while (digitalRead(BTpin) == HIGH) {
    Val = map(analogRead(pinPote), 0, 1023.0, 0, 255);
    digitalWrite(pinLedBT, HIGH);
    if (mySerial.read() == 'A') {
      pote = mySerial.parseInt();
      if (pote < 10) pote = 0;
      if (pote > 242) pote = 255;
      analogWrite(pinLed, pote);
    }

    if (mySerial.read() == '\n') {
       pote = pote;

    }
    if ((Val > ValOld + 3 ) or (Val < ValOld - 3 )) break;
  }
  if (digitalRead(BTpin) == LOW) {
    digitalWrite(pinLedBT, LOW);
  }
}


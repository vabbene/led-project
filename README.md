![alt text](https://gitlab.com/vabbene/led-project/raw/master/Im%C3%A1genes/Vabbene%20LEDPROJECT.jpg "Vabbene Led Project Patagonia")


# Descripción
Los reflectores antiguos de cine o teatro hoy día son muy utilizados en decoraciones de estilo industrial por su llamativo aspecto y diseño.

Té ofrecemos un reflector de iluminación marca FadaF de la empresa Lujan & cia. S.R.L. empleado en el mundo del cine y el teatro como así también en la fotografía. El cual ha sido restaurado a color negro, conservando todos sus elementos originales, salvo el sistema eléctrico que fue modificado para adaptarlo a la tecnología led, ofreciendo así una buena iluminación y un consumo eléctrico extremadamente moderado, en comparación a las viejas lamparas de filamento.

![alt text](https://gitlab.com/vabbene/led-project/raw/master/Im%C3%A1genes/parte1.jpg "Vabbene Led Project Patagonia")

El sistema que hemos empleado para la iluminación, no utiliza bombillas led convencionales, sino 5 led's de 10w de alta potencia conectados en paralelo y que gracias a la tecnología de arduino nos permite brindar la posibilidad de atenuar la cantidad de iluminación en forma física, como así también, vía bluetooth hacerlo de manera inalámbrica, por medio de nuestra exclusiva aplicación creada para el sistema operativo Android. De esta forma, con tu smartphone podrás controlar el grado de intensidad lumínica del reflector dentro de un rango de distancia un poco mayor a los 10m.

Te recordamos que estos spot han sido fabricadas para usos específicos y no para ser utilizadas como luz principal en una estancia. 

![alt text](https://gitlab.com/vabbene/led-project/raw/master/Im%C3%A1genes/parte8.jpg "Vabbene Led Project Patagonia")

Dentro de las características podemos destacar es que en la parte frontal contiene un cristal gofrado que proyecta su luz de forma difuminada en un radio focalizado, y en las partes inferior, superior y trasera contiene agujeros necesarios para la adecuada ventilación, por lo que se aconseja mantener en espacios protegidos donde no se obstaculice dicha necesidad de circulación de aire.

Si te gusta el estilo industrial, no te pierdas esta magnífica oportunidad de poder poseer e incluir en tu decoración esta genial lámpara restaurada que mantiene una interesante combinación tecno-retro-industrial.







